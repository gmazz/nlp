# -*- coding: UTF-8 -*-
from StringIO import StringIO

import nltk
import xmlparser
import model
import codecs
import tagger
import xml.etree.cElementTree as ET

import xml.dom.minidom


# Open XML file (A simple 'open' is the easiest way to obtain inputText compatible with xmlparser.py)
InFile = open('input.xml')
lines = InFile.readlines()
inputText=""
for ln in lines:
    inputText=inputText+ln

### parsing XML into the object structure
text=xmlparser.parseText(inputText)
### aggregate the sentences to single list to simplify processing (structure is preserved by references)
sentences=[]
for paragraph in text:
    sentences=sentences+paragraph.sentences

tagger.tagToken(sentences)
textRoot = ET.Element("text")

for paragraph in text:
    pNode = ET.SubElement(textRoot, "p")
    pNode.set("xml:id",paragraph.idnt)
    for sentence in paragraph.sentences:
        sNode=ET.SubElement(pNode, "s")
        sNode.set("xml:id",sentence.idnt)
        segmentCounter=1
        for segment in sentence.getTokenization().inferred:
            segNode=ET.SubElement(sNode, "seg")
            segNode.set("xml:id",sentence.idntPrefix+"-"+str(segmentCounter)+"-"+"seg")
            token=ET.SubElement(segNode, "token")
            token.text=segment.text
            tag=ET.SubElement(segNode, "tag")
            ### heck, the HACK (should fix by extending the *MatchedSpans, no time)
            if segment.tag=="DATE":
                tag.text=segment.value
            else:
                tag.text=segment.tag
            pass
            segmentCounter+=1
            pass
        ### segmentation
        pass

tree = ET.ElementTree(textRoot)
zz=StringIO()
tree.write(zz)

#print zz.getvalue()

xml = xml.dom.minidom.parseString(zz.getvalue()) # or xml.dom.minidom.parseString(xml_string)
pretty_xml_as_string = xml.toprettyxml()

print pretty_xml_as_string
tree.write("output.xml")