# -*- coding: UTF-8 -*-
import re
import model
import string

from nltk import tokenize

### Write a program that classifies elements in Polish texts into several groups: 
##words (word), 
#Arabic numerals (ara), 
#Roman numerals (rom), 
#abbreviations (abbrev), 
#punctuation marks (punct), 
#www addresses (www), 
#e-mail addresses (email), 
#dates written using numbers, e.g., 14.03.2014, or text, e.g., '14 marca 2014 r.' (rrrr.mm.dd).

class TAGS:
    ARABIC="ara"
    ROMAN="rom"
    ABBREV="abbrev"
    PUNCTUATION="punct"
    WWW="www"
    EMAIL="email"
    DATE1="DATE1"
    DATE2="DATE2"
    UNKNOWN="UNK"
    WORD="word"


'''
This function return the date in the yyyy, mm, dd format.
It can be used directly by tagToken to re-format the date.
Since the position of dateRule_txt, dateRule_nmb are known (REGEX_RULES[2] and REGEX_RULES[3])
the FLAG_DataType will be no more required cause tagToken will directly handle the different cases.[
'''

date_assoc = {
    u'stycznia':'01',
    u'styczeń':'01',
    u'styczniu':'01',
    u'lutego':'02',
    u'luty':'02',
    u'lutym':'02',
    u'marca':'03',
    u'marzec':'03',
    u'marcu':'03',
    u'kwietnia':'04',
    u'kwiecień':'04',
    u'kwietniu':'04',
    u'maja':'05',
    u'maj':'05',
    u'czerwca':'06',
    u'czerwiec':'06',
    u'czerwcu':'06',
    u'lipca':'07',
    u'lipcu':'07',
    u'lipiec':'07',
    u'sierpnia':'08',
    u'sierpień':'08',
    u'sierpniu':'08',
    u'września':'09',
    u'wrzesień':'09',
    u'wrześniu':'09',
    u'października':'10',
    u'październiku':'10',
    u'październik':'10',
    u'listopada':'11',
    u'listopadzie':'11',
    u'listopad':'11',
    u'grudnia':'12',
    u'grudniu':'12',
    u'grudzień':'12'
}

wordPunctTokenizer = tokenize.WordPunctTokenizer()

def textDateExtractor(patt):
    dd, mm, yy, r = patt.groups()
    yy = yy_trans(yy)
    dd = mmdd_trans(dd)
    for k,v in date_assoc.items():
        if mm in k:
            mm_r = v
            date_r = '%s.%s.%s' %(yy, mm_r, dd)
            return date_r
        else:
            pass

def numDateExtractor(patt):
    if patt:
        dd, mm, yy, r = patt.groups()

        dd = mmdd_trans(dd)
        mm = mmdd_trans(mm)
        yy = yy_trans(yy)

        date_r = '%s.%s.%s' %(yy, mm, dd)
        return date_r

def mmdd_trans(md):
    if len(md) < 2:
        md = '0'+md
    elif 'trzeci' in md:
        md = '03'
    return md

def yy_trans(yy):
    if len(yy) < 4:
        yy = '19'+ yy
    return yy

## rule for e-mails
#### based on http://stackoverflow.com/questions/8022530/python-check-for-valid-email-address     
emailRule={'regexp':'[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]{2,4}','tag':TAGS.EMAIL}
## rule for Roman numerals
### based on http://my.safaribooksonline.com/book/programming/regular-expressions/9780596802837/6dot-numbers/id3015181
romanRule={'regexp':'[MDCLXVI]+','tag':TAGS.ROMAN} ##
## rule for text date format
### based on http://www.regular-expressions.info/numericranges.html
dateRule_txt={'regexp':'([0-2]?[1-9]|[3][0-1]|[A-Za-zź]+)\s([A-Za-zź]+)\s(\d{4}|\d{2})\s?([r.]+)?','tag':TAGS.DATE1,'postProcess':textDateExtractor}
dateRule_nmb={'regexp':'([0-2]?[1-9]|[3][0-1])[/.-]([0][1-9]|[1][0-2])[/.-](\d{4}|\d{2})\s?([r.]+)?[?]?','tag':TAGS.DATE2,'postProcess':numDateExtractor}
wwwRule={'regexp':'^(?:http://|www.)[^"]+','tag':TAGS.WWW}
#punctuationRule={'regexp':'(?![\._])\p{P}','tag':TAGS.PUNCTUATION}
punctuationRule={'regexp':'([%s\\\-]+)'%string.punctuation,'tag':TAGS.PUNCTUATION}
arabicRule={'regexp':r'[+-]? *(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][+-]?\d+)?','tag':TAGS.ARABIC}
## TODO: add https, add query part (after #, or ? in url)


REGEX_RULES=[
       emailRule,
       romanRule,
       dateRule_txt,
       dateRule_nmb,
       wwwRule,
       punctuationRule,
       arabicRule
       ]

## inverted dict based on tags (assumes unique tags)
REGEX_RULES_INV_DICT={}

## compilation of regex rules
REGEX_RULES_COMP=[]
for rule in REGEX_RULES:
    REGEX_RULES_COMP.append({'regexp':re.compile(rule['regexp']),'tag':rule['tag']})
    REGEX_RULES_INV_DICT[rule['tag']]=rule

def tagSentence(sen):
    tokenTags = model.Totem()
    for rule in REGEX_RULES_COMP:
        m = (rule['regexp']).finditer(sen.text)
        if m:
            # for each match
            for x in m:
                tokenTags.addElem(model.RegExMatchedSpan(x,rule['tag']))
                pass
    #general tagging
    z=[model.MatchedSpan(z,sen.text[z[0]:z[1]],TAGS.UNKNOWN) for z in wordPunctTokenizer.span_tokenize(sen.text)]
    tokenTags.addElems(z)
    sen.addTokenization(tokenTags)


def processTagging(sen):
    for tgName, tgList in sen.getTokenization().alldict.items():
        #print tgName,tgList
        theRule=REGEX_RULES_INV_DICT.get(tgName)
        if theRule!=None and theRule.has_key('postProcess'):
            ppLambda= theRule['postProcess']
            for tgMatch in tgList:
                tgMatch.value=ppLambda(tgMatch.match)

def tagToken(sentences):
    for sen in sentences:
        tagSentence(sen)
        processTagging(sen)
        AbbrevMatching(sen)
        sen.getTokenization().inferTokens()

def AbbrevMatching(sen):

    abbrev_pun = []
    abb_pun = []
    abb_npun = []

    abbrev_file = open('abbrev.txt', 'r')
    lines = abbrev_file.readlines()
    for l in lines:
        l = l.strip()
        tmp_abb = l.split('\t')
        if 'brev:pun' in  tmp_abb[2]:
            abb_pun.append(tmp_abb[0].decode('utf-8'))
        elif 'brev:npun' in tmp_abb[2]:
            abb_npun.append(tmp_abb[0].decode('utf-8'))

    AbbrevReader(abb_pun, sen)
    AbbrevReader(abb_npun, sen)

def AbbrevReader(abb_list, sen):
    for abb in abb_list: # Making  pun/npun separate for flexibility (but maybe not necessary)
        #m = re.compile('\\b%s\\b[.]?'%abb, re.UNICODE) # 'abbrev.' version
        m = re.compile('\\b%s\\b'%abb, re.UNICODE) # 'abbrev' '.'(punct) version
        results = m.finditer(sen.text)
        if results:
            for x in results:
                sen.getTokenization().addElem(model.RegExMatchedSpan(x,TAGS.ABBREV))

if __name__ == '__main__':
    pass