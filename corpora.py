from nltk.corpus import pl196x
import os
from xmlparser import parseFileToObj,parseFileToObj2
from lxml import etree, objectify


#print len(pl196x.tagged_words())

def pl196x_abbrev():
    alltags=set()
    abbrevWords=set()
    for wordtag in pl196x.tagged_words():
        word=wordtag[0]
        tagging=wordtag[1]
        tagSet=set(tagging.split(" "))
        isAbbrev=[txt.endswith("S") for txt in tagSet]
        hasAbbrev=reduce(lambda x,y: x or y, isAbbrev)
        if hasAbbrev:
            abbrevWords.add(word)
            pass
        alltags.update(tagSet)
        
    print 1

def processWordAnn(fPath):
    z=parseFileToObj(fPath)
    #print z.TEI.text
    root=z.getroot()
    mmmm=objectify.dump(root)
    print(mmmm)
    mm=root.TEI
    print mm
    print root
    for elem in root:
        print elem
    print z
    pass

def processWordAnn2(fPath):
    z=parseFileToObj2(fPath)
    root=z.getroot()
    for segFs in root.iter('{http://www.tei-c.org/ns/1.0}fs'):
        #for fPart in seg.iter
        fSubDict={}
        for fSub in segFs:
            key=fSub.attrib['name']
            if key=="orth" or key=="base":
                fSubDict[key]=fSub[0].text
            elif key=="ctag" or key=="msd":
                fSubDict[key]=fSub[0].attrib['value']
        print u"\t".join([fSubDict["orth"],fSubDict["base"],fSubDict["ctag"],fSubDict["msd"],]).encode('utf-8')   
    
    pass


def NKJP_abbrev():
    toProcess=[]
    for root, dirs, files in os.walk("/Users/cezden/Downloads/NKJP-PodkorpusMilionowy-1.2"):
        if 'ann_words.xml' in files:
            toProcess.append(os.path.join(root,'ann_words.xml'))
    print len(toProcess)
    for fPath in toProcess:
        processWordAnn2(fPath)
if __name__ == '__main__':
    NKJP_abbrev()
    
    
    
    