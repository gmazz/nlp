# -*- coding: UTF-8 -*-
from lxml import etree, objectify
from model import Paragraph
import xml.etree.ElementTree as ET

__magical_parser2 = objectify.makeparser(encoding='utf-8', recover=True)


def parseToObj(response):
    return objectify.fromstring(response, __magical_parser2)

def parseFileToObj(fPath):
    f=open(fPath)
    retObj=objectify.parse(f, __magical_parser2)
    f.close()
    return retObj
    #return objectify.fromstring(response, __magical_parser2)

def parseFileToObj2(fPath):
    retObj=ET.parse(fPath)
    return retObj

def parseText(inputText):
    z=parseToObj(inputText)
    text=[]
    for paragraph in z.p: #.p is each paragraph of the XML structure
        text.append(Paragraph.fromObjectified(paragraph))
    return text #list of objects (a[i.text] to get the text)
