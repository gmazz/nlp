import tags

class Paragraph:
    def __init__(self,idnt):
        self.sentences=[]
        self.idnt=idnt
        pass
    def addSentence(self,sentence):
        self.sentences.append(sentence)
        
    @staticmethod
    def fromObjectified(objElem):
        idnt=objElem.attrib['{http://www.w3.org/XML/1998/namespace}id']
        par=Paragraph(unicode(idnt))
        for sentence in objElem.s:
            par.addSentence(Sentence.fromObjectified(sentence))
        return par 
        pass

class Sentence:
    def __init__(self,idnt,text):
        self.text=text
        self.idnt=idnt
        self.idntPrefix="-".join(idnt.split("-")[0:2])
        self.tokenization = None
        pass
    @staticmethod
    def fromObjectified(objElem):
        idnt=objElem.attrib['{http://www.w3.org/XML/1998/namespace}id']
        text=unicode(objElem.text)
        return Sentence(idnt,text)
    def addTokenization(self,tok):
        self.tokenization=tok

    def getTokenization(self):
        return self.tokenization

class TagResolver:
    PRECEDENCE=[tags.WWW,tags.EMAIL,tags.DATE1,tags.DATE2,tags.DATE,tags.ARABIC,tags.ROMAN,tags.ABBREV,tags.PUNCTUATION,tags.WORD,tags.UNKNOWN]
    @staticmethod
    def resolve(tagList):
        tokenDict={}
        for token in tagList:
            tokenDict[token.tag]=token
        for prec in TagResolver.PRECEDENCE:
            val=tokenDict.get(prec)
            if val!=None:
                return val
        pass

class MatchedSpan:
    def __init__(self,span,text,tag):
        self.value=text
        self.text=text
        self.span=span
        self.tag=tag
    def getSpan(self):
        return self.span
    def getBegin(self):
        return self.getSpan()[0]
    def cmpInside(self,oth):
        #testing for overlap
        overlap= max(0, min(self.span[1], oth.span[1]) - max(self.span[0], oth.span[0]))
        if overlap==0:
            return None
        ## we do have some overlap, is one inside the other?
        len1=self.span[1]-self.span[0]
        len2=oth.span[1]-oth.span[0]
        selfInOth=(len1==overlap)
        othInSelf=(len2==overlap)
        ## lets handle equality first:
        if selfInOth and othInSelf:
            #now we need to decide about tag precedence
            return None  ## TODO change!
            pass 
        if selfInOth:
            return -1
        else:
            return 1
        
        
    

class RegExMatchedSpan(MatchedSpan):
    def __init__(self,matchObj,tag):
        span=matchObj.span()
        txt=matchObj.string[span[0]:span[1]]
        MatchedSpan.__init__(self, span, txt, tag)
        self.value=None
        self.match=matchObj
        pass

class Totem:
    def __init__(self):
        self.alldict = {}
        self.inferred=None

    def addElem(self, m):
        if self.alldict.has_key(m.tag):
            self.alldict[m.tag].append(m)
        else:
            self.alldict[m.tag]=[m]
        pass
    
    def addElems(self,listOfElems):
        for m in listOfElems:
            self.addElem(m)
        pass
    
    
    def inferTokens(self):
        allTokens=[]
        for tokenList in self.alldict.values():
            allTokens=allTokens+tokenList
        sorted(allTokens, key=lambda token: token.getBegin())
        if len(allTokens)>0:
            validToken=[True]*len(allTokens)
            for idx1 in range(0,len(allTokens)-1):
                if validToken[idx1]:
                    token1=allTokens[idx1]
                    for idx2 in range(idx1+1,len(allTokens)):
                        ## compare tokens
                        if validToken[idx2]:
                            token2=allTokens[idx2]
                            cmpVal=token1.cmpInside(token2)
                            if cmpVal!=None:
                                if cmpVal<0:
                                    ## token1 inside token2
                                    validToken[idx1]=False
                                    break
                                else:
                                    validToken[idx2]=False
            ## now we can safely remove invalid tokens
            self.alldict = {}
            inferred=[]
            for idx1 in range(len(allTokens)):
                if validToken[idx1]:
                    self.addElem(allTokens[idx1])
                    inferred.append(allTokens[idx1])
            inferred=sorted(inferred, key=lambda token: token.getBegin())
            ## we assume that tokens can occur multiple times, but tokens with equal begins of the span must have equal ending
            tDict={}
            for token in inferred:
                k=token.getBegin()
                if tDict.has_key(k):
                    tDict[k].append(token)
                else:
                    tDict[k]=[token]
                pass
            inferred=[]
            for k,v in tDict.items():
                if len(v)==1:
                    ## no disambiguity
                    inferred.append(v[0])
                else:
                    inferred.append(TagResolver.resolve(v))
            self.inferred=sorted(inferred, key=lambda token: token.getBegin())
            finalMapping={tags.UNKNOWN:tags.WORD, tags.DATE1:tags.DATE, tags.DATE2:tags.DATE}
            for token in self.inferred:
                substToken=finalMapping.get(token.tag)
                if substToken!=None:
                    token.tag=substToken
            #print self.inferred
        pass


    #def find (self, mtag):
    #    self.mtag = mtag
    #    for i in self.alldict[self.mtag]:
    #        print i